package com.sbgit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbgitApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbgitApplication.class, args);
	}

}
